

def get_api_key_from_file(filename="apikey.txt"):
    """Read API key from the file"""
    try:
        f = open(filename, "r")
    except FileNotFoundError:
        print(f"File {filename} that should contain API key was not found!")
        return ""
    line = f.readline()
    return line