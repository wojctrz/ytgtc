from PIL import Image, ImageStat, ImageDraw

import math

pic = Image.open("MovieCreator/example_pictures/the_great_potato_race/1.png")

# pic.show()

# mask = Image.new("L", pic.size, 0)

width, height = pic.size

height_third = math.floor(height/3)

north_mask = Image.new("L", pic.size, 0)
north_mask_draw = ImageDraw.Draw(north_mask)
north_mask_draw.rectangle((0, 0, width, height_third), fill=255)

center_mask = Image.new("L", pic.size, 0)
center_mask_draw = ImageDraw.Draw(center_mask)
center_mask_draw.rectangle((0, height_third, width, height_third * 2), fill=255)

south_mask = Image.new("L", pic.size, 0)
south_mask_draw = ImageDraw.Draw(south_mask)
south_mask_draw.rectangle((0, height_third * 2, width, height_third * 3), fill=255)

# draw = ImageDraw.Draw(mask)
# draw.rectangle((0, 700, 1024, 1024), fill=255)
# mask.show()

north_stats = ImageStat.Stat(pic, north_mask)
north_variance = north_stats.var
print(sum(north_variance))

center_stats = ImageStat.Stat(pic, center_mask)
center_variance = center_stats.var
print(sum(center_variance))

south_stats = ImageStat.Stat(pic, south_mask)
south_variance = south_stats.var
print(sum(south_variance))