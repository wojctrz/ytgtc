from oaikeygetter import get_api_key_from_file
from StoryCreator.StoryCreatorFactory import StoryCreatorFactory
from MovieCreator.MovieCreatorFactory import MovieCreatorFactory

API_KEY = get_api_key_from_file()



story_creator = StoryCreatorFactory().get_mock_story_creator(API_KEY, "6.txt")

xd = story_creator.get_story("Tell me a funny story. Story should be about 1000 words long and have a title")



movie_creator = MovieCreatorFactory().get_basic_movie_creator(xd)

movie_filename = xd.title.replace(" ", "_") + ".mp4"
movie_creator.create_movie(movie_filename)

print(xd.title)
print(xd.content)
