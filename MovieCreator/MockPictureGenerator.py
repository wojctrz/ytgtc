from .IPictureGenerator import IPictureGenerator

import os

class MockPictureGenerator(IPictureGenerator):

    def __init__(self, story):
        super().__init__(story)
        self.story_title_lowercase_no_whitespace = self.story.title.replace(" ", "_").lower().strip()

    def generate_pictures(self, num_pictures):
        """As a mock, just return paths to all pictures from the example_pictures
        directory that is specific to the title of the story.
        If folder with pictures for the specific story does not exist, return some
        generalized mock pictures.

        """
        return self._get_story_specific_pictures()


    def _get_story_specific_pictures(self):
        dir_name = "MovieCreator/example_pictures"
        try:
            filenames = os.listdir(os.path.join(dir_name, self.story_title_lowercase_no_whitespace))
            files = [os.path.join(dir_name, self.story_title_lowercase_no_whitespace, f) for f in filenames]
        except FileNotFoundError:
            filenames = [f for f in os.listdir(dir_name) if os.path.isfile(os.path.join(dir_name, f))]  # Get only files, ignore directories
            files = [os.path.join(dir_name, f) for f in filenames]
        return files
