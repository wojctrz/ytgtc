from abc import ABC, abstractmethod


class IStoryDivider(ABC):
    """Interface class for dividing story into displayable chunks"""

    def __init__(self, story):
        self.story = story

    @abstractmethod
    def get_next_chunk(self):
        pass
