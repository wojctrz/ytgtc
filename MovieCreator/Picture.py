import math

from PIL import Image, ImageStat, ImageDraw

class Picture:

    @property
    def path(self):
        return self._path

    def __init__(self, path):
        self._path = path
        self._subtitle_placement = None  # It is lazy evaluated in get_best_subtitle_placement

        self._open_picture()
        self._create_masks()

    def get_best_subtitle_placement(self):
        if self._subtitle_placement is not None:
            return self._subtitle_placement

        variances = self._get_all_variances()
        min_variance_value = [k for k,v in variances.items() if v == min(variances.values())][0]

        self._subtitle_placement = min_variance_value
        return self._subtitle_placement

    def _open_picture(self):
        self._picture = Image.open(self._path)
        self._width, self._height = self._picture.size
        self._height_third = math.floor(self._height / 3)

    def _get_all_variances(self):
        north_variance = self._get_north_variance()
        center_variance = self._get_center_variance()
        south_variance = self._get_south_variance()

        variances = {"top": north_variance, "center": center_variance, "bottom": south_variance}

        return variances

    def _create_masks(self):
        self._north_mask = Image.new("L", self._picture.size, 0)
        north_mask_draw = ImageDraw.Draw(self._north_mask)
        north_mask_draw.rectangle((0, 0, self._width, self._height_third), fill=255)

        self._center_mask = Image.new("L", self._picture.size, 0)
        center_mask_draw = ImageDraw.Draw(self._center_mask)
        center_mask_draw.rectangle((0, self._height_third, self._width, self._height_third * 2), fill=255)

        self._south_mask = Image.new("L", self._picture.size, 0)
        south_mask_draw = ImageDraw.Draw(self._south_mask)
        south_mask_draw.rectangle((0, self._height_third * 2, self._width, self._height_third * 3), fill=255)

        self._all_masks = {"top": self._north_mask, "center": self._center_mask, "bottom": self._south_mask}

    def _get_north_variance(self):
        north_stats = ImageStat.Stat(self._picture, self._north_mask)
        north_variance = north_stats.var
        return sum(north_variance)

    def _get_center_variance(self):
        center_stats = ImageStat.Stat(self._picture, self._center_mask)
        center_variance = center_stats.var
        return sum(center_variance)

    def _get_south_variance(self):
        south_stats = ImageStat.Stat(self._picture, self._south_mask)
        south_variance = south_stats.var
        return sum(south_variance)