import math

from .IMovieCreator import IMovieCreator
from .EndOfStoryError import EndOfStoryError
from .Picture import Picture

from moviepy.editor import *


class BasicMovieCreator(IMovieCreator):

    def __init__(self, story, story_divider, picture_generator):
        super().__init__(story, story_divider, picture_generator)
        self.SIZE = (1024, 720)
        self.REQUESTED_NUM_PICTURES = 4
        self.STANDARD_FRAME_DURATION_S = 5
        self.TEXT_FIELD_OPACITY = 0.65
        self.movie_clips = list()
        self.text_frames = list()

    def create_movie(self, filename):
        self._create_title_frame()

        self._create_story_frames()

        final = concatenate_videoclips(self.movie_clips)

        final.write_videofile(filename, fps=24)

    def _create_story_frames(self):
        self._get_pictures(self.REQUESTED_NUM_PICTURES)
        story_chunks = list()
        while True:
            try:
                next_chunk = self.divider.get_next_chunk()
                story_chunks.append(next_chunk)
            except EndOfStoryError:
                break
        num_text_frames = len(story_chunks)
        num_pictures = len(self.pictures_paths)
        num_text_frames_per_pic = math.ceil(num_text_frames / num_pictures)

        pics = [Picture(path) for path in self.pictures_paths]

        splitted_chunks = self._split_chunks_list_for_each_pic(story_chunks, num_text_frames_per_pic)
        for pic, chunks in zip(pics, splitted_chunks):
            text_placement = pic.get_best_subtitle_placement()
            subtitle_color = "black"
            background_color = f"rgba(255,255,255, {self.TEXT_FIELD_OPACITY})"
            for chunk in chunks:
                chunk = self._preprocess_text_chunk(chunk, text_placement)
                sentence_frame = TextClip(chunk, color=subtitle_color,
                                bg_color=background_color, font="Century-Schoolbook-Italic",
                                fontsize=40, method="caption", size=(self.SIZE[0], 0)
                                ).set_duration(self.STANDARD_FRAME_DURATION_S).set_position(text_placement)
                image_clip = self._get_image_clip(pic.path)
                next_clip = CompositeVideoClip([image_clip, sentence_frame])
                self.movie_clips.append(next_clip)

    def _preprocess_text_chunk(self, chunk, chunk_placement):
        """Trim trailing whitespace if any.

        Additionally, if the chunk is going to be placed at the bottom,
        add some whitespace after it, to display it higher and not be
        covered by the playback controls.

        """
        stripped_chunk = chunk.strip()
        if chunk_placement == "bottom":
            stripped_chunk += "\n\n"

        return stripped_chunk

    def _split_chunks_list_for_each_pic(self, chunks, chunks_per_pic):
        splitted_list = list()
        for i in range(0, len(chunks), chunks_per_pic):
            splitted_list.append(chunks[i:i+chunks_per_pic])
        return splitted_list

    def _create_title_frame(self):
        title = TextClip(self.story.title, color="MediumOrchid4", size=self.SIZE,
                         bg_color="black", font="AvantGarde-BookOblique", fontsize=60, method="caption").set_duration(10)
        self.movie_clips.append(title)

    def _get_pictures(self, num_pictures):
        self.pictures_paths = self.pic_generator.generate_pictures(num_pictures)

    def _get_image_clip(self, image_path):
        return ImageClip(image_path).resize(self.SIZE).set_duration(self.STANDARD_FRAME_DURATION_S)