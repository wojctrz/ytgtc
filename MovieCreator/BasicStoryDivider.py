from .IStoryDivider import IStoryDivider
from .EndOfStoryError import EndOfStoryError

class BasicStoryDivider(IStoryDivider):

    def __init__(self, story):
        super().__init__(story)
        self.divided_story = self.story.content.split(".")
        self.idx = 0

    def get_next_chunk(self):
        try:
            chunk = self._get_next_sentence()
        except EndOfStoryError:
            raise
        while(len(chunk) < 100):
            try:
                next_sentence = self._get_next_sentence()
            except EndOfStoryError:
                break
            chunk += next_sentence
        return chunk

    def _get_next_sentence(self):
        try:
            sentence = self.divided_story[self.idx] + "."
        except IndexError:
            raise EndOfStoryError
        self.idx += 1
        return sentence
