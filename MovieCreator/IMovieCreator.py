from abc import ABC, abstractmethod


class IMovieCreator(ABC):
    """Interface class for creating movies"""

    def __init__(self, story, story_divider, picture_generator):
        self.story = story
        self.divider = story_divider
        self.pic_generator = picture_generator

    @abstractmethod
    def create_movie(self, filename):
        pass
