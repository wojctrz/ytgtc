from abc import ABC, abstractmethod


class IPictureGenerator(ABC):
    """Interface class for Generating Pictures based on story title"""

    def __init__(self, story):
        self.story = story

    @abstractmethod
    def generate_pictures(self, num_pictures):
        pass
