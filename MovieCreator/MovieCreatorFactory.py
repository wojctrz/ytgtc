from .BasicMovieCreator import BasicMovieCreator
from .BasicStoryDivider import BasicStoryDivider
from .MockPictureGenerator import MockPictureGenerator

class MovieCreatorFactory():

    def get_basic_movie_creator(self, story):
        divider = BasicStoryDivider(story)
        pic_generator = MockPictureGenerator(story)
        creator = BasicMovieCreator(story, divider, pic_generator)
        return creator
