import openai

from .IStoryCreator import IStoryCreator
from .Story import Story

class OAIStoryCreator(IStoryCreator):

    def __init__(self, api_key):
        openai.api_key = api_key

        self.engine = None
        self.tokens = 0
        self.temperature = 0

    def get_story(self, description):
        # Generate a response
        completion = self._get_oai_completion(description)

        story = self._extract_story_title(completion)
        return story

    def set_completion_parameters(self, engine, max_tokens, temperature):
        self.engine = engine
        self.tokens = max_tokens
        self.temperature = temperature

    def _get_oai_completion(self, prompt):
        completion = openai.Completion.create(
        engine=self.engine,
        prompt=prompt,
        max_tokens=self.tokens,
        n=1,
        stop=None,
        temperature=self.temperature,
        )

        return completion.choices[0].text

    
    def _extract_story_title(self, text):
        # Let's assume that the first line is the title
        story = Story
        text = text.strip()  # Remove trailing whitespace if any
        splitted_text = text.split("\n", 1)
        story.title = splitted_text[0]
        story.title.strip()  # Trim any trailing whitespace
        story.content = splitted_text[1]
        story.content.strip()
        return story
