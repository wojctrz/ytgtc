import os
import random

from .IStoryCreator import IStoryCreator
from .Story import Story

class MockStoryCreator(IStoryCreator):
    """Class for getting some stories from the disk.

    It is useful for testing, when we want to avoid sending
    requests to OpenAI and paying money

    """

    def __init__(self, api_key, filename):
        """Create MockstoryCreator.
        
        Filename can be set to None if you want to get random story
        """
        self.filename = filename

    def get_story(self, description):
        if self.filename is None:
            try:
                file = self._get_random_story_file()
            except FileNotFoundError:
                print(f"Directory with example stories not found!")
                raise
        else:
            file = os.path.join("StoryCreator", "example_stories", self.filename)

        try:
            f = open(file, "r")
        except FileNotFoundError:
            print(f"File with example story not found!")
            raise
        lines = f.readlines()

        story = Story
        story.title = lines[0]
        story.title.strip()  # Trim any trailing whitespace
        story.content = "".join(lines[1:])
        story.content.strip()

        return story

    def _get_random_story_file(self):
        all_stories = os.listdir("StoryCreator/example_stories")
        story_filename = random.choice(all_stories)
        story_path = os.path.join("StoryCreator", "example_stories", story_filename)
        return story_path
