from .MockStoryCreator import MockStoryCreator
from .OAIStoryCreator import OAIStoryCreator

class StoryCreatorFactory():

    DEFAULT_ENGINE = "text-davinci-003"
    DEFAULT_TOKENS = 1024
    DEFAULT_TEMPERATURE = 0.5

    def get_mock_story_creator(self, api_key, filename=None):
        return MockStoryCreator(api_key, filename)

    def get_oai_story_creator(self, api_key, engine=DEFAULT_ENGINE, max_tokens=DEFAULT_TOKENS, temperature=DEFAULT_TEMPERATURE):
        creator = OAIStoryCreator(api_key)
        creator.set_completion_parameters( engine, max_tokens, temperature)
        return creator
