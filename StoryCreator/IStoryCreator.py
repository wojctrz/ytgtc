from abc import ABC, abstractmethod

from .Story import Story
class IStoryCreator(ABC):
    """Interface class for creating stories using AI"""

    @abstractmethod
    def __init__(self, api_key):
        pass

    @abstractmethod
    def get_story(self, description):
        pass
