# YTGPC

YTGPC - YouTube Generative Pre-Trained Clickbait

## Requirements
1. Python 3
    > To install python requirements, execute `pip install -r requirements.txt` from the main repo folder
1. MoviePy requires [ImageMagick](https://imagemagick.org/) to be installed
1. Probably something more, I will add more detailed documentation [when necessary](http://agilemanifesto.org/)

## How to run
1. Create `apikey.txt` and paste your OpenAI API key into it
1. Just call `python ytgtc.py`
1. If you don't want to use OpenAI API, you can use Mock Story Creator by replacing `get_oai_story_creator` method of `StoryCreatorFactory` with `get_mock_story_creator`